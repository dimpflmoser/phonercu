#!/usr/bin/env python3

import json
import dbus
import socket
import time
import threading

session_bus  = dbus.SessionBus()
voice_object = session_bus.get_object('org.nemomobile.voicecall', '/')
voice_iface  = dbus.Interface(voice_object, 'org.nemomobile.voicecall.VoiceCallManager')
sim = ['/org/freedesktop/Telepathy/Account/ring/tel/ril_0', '/org/freedesktop/Telepathy/Account/ring/tel/ril_1']

activecall_object = session_bus.get_object('org.nemomobile.voicecall', '/calls/active')
activecall_iface = dbus.Interface(activecall_object, 'org.nemomobile.voicecall.VoiceCall')

class CmdHandler(threading.Thread):

    def __init__(self, client):
        threading.Thread.__init__(self)
        self.cmdHandler = {
            "call": self.call,
            "hangup": self.hangup,
            "reject": self.reject,
            "send_sms": self.send_sms,
            "ping": self.ping,
        }

        self.socket = client[0]
        self.addr = client[1]
        self.start()

    def __del__(self):
        self.socket.close()

    def call(self, myJSON):
        #{"cmd":"call","number":"+4989636634754","SIM":1}
        print(myJSON['cmd'], myJSON["number"], myJSON["SIM"])
        ret_val = voice_iface.dial(sim[myJSON["SIM"] - 1], myJSON['number'])
        print("ret_val", ret_val)

    def hangup(self, myJSON):
        #{"cmd": "hangup"}
        print(myJSON['cmd'])
        ret_val = activecall_iface.hangup()
        print("ret_val", ret_val)

    def reject(self, myJSON):
        print(myJSON["number"], myJSON["SIM"])

    def ping(self, myJSON):
        # {"cmd":"pong"}
        print("Got ping, sending pong")
        msg = '{"cmd":"pong"}'
        self.socket.send(msg.encode())

    def send_sms(self, myJSON):
        print(myJSON["number"], myJSON["SIM"])

    def not_implemented(self, myJSON):
        print('[ERROR]', 'this command has not yet been implemented:', myJSON['cmd'])

    def run(self):
        print(self)
        while True:
            msg = self.socket.recv(4096).decode("utf-8")
            if(len(msg) == 0):
                continue
            myJSON = json.JSONDecoder().decode(msg)
            my_cmd = myJSON['cmd']
            self.cmdHandler.get(my_cmd, self.not_implemented)(myJSON)

class ServerSocket(threading.Thread):
    def __init__(self, addr_info):
        threading.Thread.__init__(self)
        #my_port = addr_info[4][1]
        self.socket = socket.socket(addr_info[0], addr_info[1], addr_info[2])
#        while True:
#            try:
#                self.socket.bind((addr_info[4][0], my_port))
#            except OSError:
#                print("[WARN]", "Address already in use", addr_info[4][0], my_port)
#                my_port += 1
#                continue
#            else:
#                break
        self.socket.bind(addr_info[4])
        self.socket.listen()
        print("[INFO]", "listening on:", addr_info[4][0], addr_info[4][1])
        self.start()

    def __del__(self):
        self.socket.close()

    def run(self):
        print("start", self)
        while True:
            CmdHandler(self.socket.accept())


# though we only select AF_INET, the resulting server socket also listens on IPv6
myaddr = socket.getaddrinfo(None, 12345, socket.AddressFamily.AF_INET, socket.SOCK_STREAM, 0, socket.AI_PASSIVE)[0]
server_socket = ServerSocket(myaddr)

print("press CTRL-c to terminate")
while True:
    time.sleep(1)
